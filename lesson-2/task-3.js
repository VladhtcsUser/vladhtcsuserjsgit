const value = 'c';

switch (value) {
    case 'a':
        console.log('a');
        break;

    case 'b':
    case 'c':
    case 'd':
    case 'e':
        console.log('others');
        break;

    default:
        console.log('unknown');
}
////////////////// Решение //////////////////
if (value === 'a') {
    console.log('a');
}
if (value === 'b' || value === 'c' || value === 'd' || value === 'e') {
    console.log('others');
}
