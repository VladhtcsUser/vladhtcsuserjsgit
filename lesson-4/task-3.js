const array = [
    false,
    'Привет.',
    2,
    'Здравствуй.',
    [],
    'Прощай.',
    {
        name: 'Уолтер',
        surname: 'Уайт',
    },
    'Приветствую.',
];

// Решение
function inspect(array) {
    const inspectArray = array.filter((item) => typeof item === 'string');
    const result = inspectArray.map((item) => item.length);
    return result
}
const result2 = inspect(array);
console.log(result2); // [ 7, 11, 7, 12 ]