const array = ['Доброе утро!', 'Добрый вечер!', 3, 512, '#', 'До свидания!'];

// Решение
function filter(array, func) {
    const newArr = [];
    if (!Array.isArray(array)) {
        throw new Error('')
    }
    if (typeof func !== 'function') {
        throw new Error('')
    }
    for (let i = 0; i < array.length; i++) {
        const element = array[i];
        const isValid = func(element, i, array);

        if (isValid) {
            newArr.push(element);
        }
    }

    return newArr;
}


const filteredArray = filter(array, function (element, index, arrayRef) {
    // console.log(`${index}:`, element, arrayRef);

    return element === 'Добрый вечер!';
});

console.log(filteredArray); // ['Добрый вечер!']