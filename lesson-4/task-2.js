const array = [1, 2, 3, 4, 5];
const INITIAL_ACCUMULATOR = 6;

// Решение


const result = array.reduce(

    function (accumulator, element, index, arrayRef) {

        // console.log(`${index}:`, accumulator, element, arrayRef);

        return accumulator + element;
    },
    INITIAL_ACCUMULATOR,
);

console.log(result); // 21