function checkSpam(source, spam) {

    if (typeof source !== 'string' || typeof spam !== 'string') {
        throw new Error('source and spam params should be string');
    }



    return source.toLowerCase().includes(spam.toLowerCase());
}

console.log(checkSpam('pitterXXX@gmail.com', 'xxx')); // true
console.log(checkSpam('pitterxxx@gmail.com', 'XXX')); // true
console.log(checkSpam('pitterxxx@gmail.com', 'sss')); // false