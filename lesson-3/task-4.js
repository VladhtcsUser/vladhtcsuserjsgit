const PRICE = '$120';

function extractCurrencyValue(source) {
    if (typeof source !== 'string') {
        return null;
    }
    const sliced = source.slice(1);
    const number = parseInt(sliced);

    return number;

}


console.log(extractCurrencyValue(PRICE)); // 120
console.log(typeof extractCurrencyValue(PRICE)); // number
console.log(extractCurrencyValue({})); // null