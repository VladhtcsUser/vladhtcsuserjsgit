const person = {};


Object.defineProperties(person, {
    rate: {
        value: 10,
        writable: true,
        configurable: false,
    },

    hours: {
        value: 10,
        writable: true,
        configurable: false,
    },

    salary: {


        get() {
            const total = this.rate * this.hours;
            return `Зарплата за проект составляет ${total}$`;
        },
    },
});


console.log(person.salary)